var express=require("express")
var bodyParser = require('body-parser')
var blog=require("./Router/Blog")
// var utilisateur=require("./Router/Utilisateur") // npm i exress -- save // creer express
var categorie=require("./Router/Categorie")
var evenements=require("./Router/Evenement")
var ville=require("./Router/Ville")
var cit=require("./Router/Citoyen")
var benevole=require("./Router/Benevole")
var contact = require("./Router/contact")
var admin=require("./Router/Admin")
var centre_interet=require("./Router/Centre_interet")
var competence=require("./Router/Competence")
var demande=require("./Router/Demande")
var groupe=require("./Router/Groupe")
var region=require("./Router/Region")
var test=require("./Router/Test")
var stats = require("./Router/stats")
var projet = require("./Router/projet")
var db=require("./Models/db")
var app=express()
var logger = require('morgan');
var cors = require('cors');


app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.set("secretKey","tokentest")

app.use("/admin", admin)
app.use("/citoyen", cit)
app.use("/users", cit);
app.use("/users", benevole);
app.use("/benevole",benevole)
app.use("/categorie",categorie)
app.use("/evenement",evenements)
app.use("/ville",ville)
app.use("/blog",blog)
app.use("/test",test)
app.use("/groupe", groupe)
app.use("region",region)
app.use("/demande",demande)
app.use("/competence",competence)
app.use("/interet",centre_interet)
app.use("/projet", projet)
app.use("/contact", contact)
app.use("/stats", stats)



app.listen(3000,function () {
    console.log("okkkk")

})
;
