var upload = require("../services/uploadService");

var express=require("express")
var Router=express.Router()
var EvenementModel=require("../Models/EvenementModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');
var path = require('path');

Router.get("/uploads/:name", (req, res) => {
    console.log(path.join(__dirname, "../public/uploads/"+ req.params.name));
    res.sendFile(path.join(__dirname, "../public/uploads/"+ req.params.name));
});

Router.post("/upload", upload);

Router.get("/all",function (req,res) {
    EvenementModel.find({},function (errr,result) {
        res.send((result))
    })
})

Router.post("/add",function (req,res) {
    evenement= new EvenementModel({
        titre: req.body.titre,
        lieu: req.body.lieu,
        detail: req.body.detail,
        nb_participant:req.body.nb_participant,
        images: req.body.images
    })
    evenement.save(function (err) {

        if(err){
            console.log(err);
            res.status(400).send({"state":"not ok","msg":"err"+err})}
        else {
            res.status(200).send({"state":"ok","msg":"ajout"})
        }

    })
});

Router.post("/participate", function (req, res) {
    EvenementModel.update({_id: req.body.id}, {$inc: { nb_participant: 1 }}, (err, data)=> {
        if(err){
            res.status(400).end();
        }
        else{
            res.status(200).json({nbrParticipant: data.nb_participant});
        }
    })
})

Router.delete("/remove/:id",function (req,res){
    EvenementModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})
Router.put("/update",function (req,res){
    EvenementModel.updateOne({id: req.body.id, titre: req.body.titre,lieu: req.body.lieu,detail: req.body.detail},
        function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})

Router.get('/:id', function (req, res) {
    EvenementModel.findById({_id: req.params.id}, (err, EvenementModel)=>{
        if(!err)
            res.status(200).json(EvenementModel)
    })
})

module.exports=Router;
