var express = require("express")
var Router = express.Router()
var AdminModel = require("../Models/AdminModel")
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all", function (req, res) {
    AdminModel.find({}, function (errr, result) {
        res.send((result))
    })
})

Router.post("/add",function (req,res) {
    admin= new AdminModel({
        nom: req.body.nom,
        prenom: req.body.prenom,
        email:req.body.email,
        mot_de_passe:req.body.password,
        login:req.body.login,
        role:req.body.role,
        groupe:req.body.groupe,
        demande:req.body.demande,

        // ville:req.body.ville,
        // blog:req.body.blog

    })
    admin.save(function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"ajout"})
        }

    })
});
Router.post("/auth",async function (req, res) {
    await AdminModel.findOne({email: req.body.email}, function (err, UserInfo) {
        if (err || !UserInfo) {
            return res.status(400).end();
        } else {

            bcrypt.compare(req.body.password, UserInfo.mot_de_passe, (err, same)=> {
                if(!err && same){
                    const token = jwt.sign({id: UserInfo._id}, req.app.get('secretKey'), {expiresIn: '300000'});
                    res.status(200).json({status: "success", message: "user found ", data: {admin: UserInfo, token: token}})
                }
                else {
                    res.status(401).json({status: "error", message: "invalid email / password ", data: null});
                }
            })
        }
    })
})

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            res.json({status: "error", message: err.message, data: null});
        } else {
            // add user id to request
            req.body.UserId = decoded.id;
            next();
        }
    });

}

Router.delete("/remove/:id", function (req, res) {
    AdminModel.deleteOne({_id: req.params.id}, function (err) {

        if (err)
            res.send({"state": "not ok", "msg": "err" + err})
        else {
            res.send({"state": "ok", "msg": "suprimee"})
        }

    })
})
Router.put("/update", function (req, res) {
    AdminModel.findByIdAndUpdate({_id:req.body.id},{
            nom: req.body.nom,
            prenom: req.body.prenom,
            email:req.body.email,
            mot_de_passe:req.body.mot_de_passe,
            login:req.body.login,
            role:req.body.role,groupe:req.body.groupe,

        },
        function (err) {

            if (err)
                res.send({"state": "not ok", "msg": "err" + err})
            else {
                res.send({"state": "ok", "msg": "modifier"})
            }

        })
})
// Router.get("/citoyen",function (req,res) {
//     UtilisateurModel.find( { "type": "citoyen" },function (err,result)
//     {res.send(result)
//     })
// })

module.exports = Router;
