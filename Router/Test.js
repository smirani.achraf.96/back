const express=require("express");
const router=express.Router();
const Test=require('../Models/TestModel');
const Joi=require('joi');



router.post("/publier",function(req,res){
    const test=new Test({
        Question1:{
            ContenuQes1:req.body.ContenuQes1,
            PropositionsQes1:[req.body.Prop1Qes1,req.body.Prop2Qes1,req.body.Prop3Qes1],
            ReponseQes1:req.body.ReponseQes1

        },


        Question2:{
            ContenuQes2:req.body.ContenuQes2,
            PropositionsQes2:[req.body.Prop1Qes2,req.body.Prop2Qes2,req.body.Prop3Qes2],
            ReponseQes2:req.body.ReponseQes2

        },


        Question3:{
            ContenuQes3:req.body.ContenuQes3,
            PropositionsQes3:[req.body.Prop1Qes3,req.body.Prop2Qes3,req.body.Prop3Qes3],
            ReponseQes3:req.body.ReponseQes3
        }  ,


        Question4:{
            ContenuQes4:req.body.ContenuQes4,
            PropositionsQes4:[req.body.Prop1Qes4,req.body.Prop2Qes4,req.body.Prop3Qes4],
            ReponseQes4:req.body.ReponseQes4

        },


        Question5:{
            ContenuQes5:req.body.ContenuQes5,
            PropositionsQes5:[req.body.Prop1Qes5,req.body.Prop2Qes5,req.body.Prop3Qes5],
            ReponseQes5:req.body.ReponseQes5

        }


    });
    test.save(function(err) {
        if(err){
            res.send(err.message);

        }
        else{
            res.send({state:'ajouté'});
        }
    });

});






router.post("/passerTest",function(req,res){
    var score=0;
    Test.findOne({_id:req.body.id},function(err,result){
        if(err){
            res.send({"state":"not found"});
        }

        if(req.body.repQes1=== result.Question1.ReponseQes1){
            score+=1;
        }
        if(req.body.repQes2=== result.Question2.ReponseQes2){
            score+=1;
        }
        if(req.body.repQes3=== result.Question3.ReponseQes3){
            score+=1;
        }
        if(req.body.repQes4=== result.Question4.ReponseQes4){
            score+=1;
        }
        if(req.body.repQes5=== result.Question5.ReponseQes5){
            score+=1;
        }
        res.json({"score":score});
    })




});









router.put("/modifier",function(req,res){
    var {error}=validateUpdatedFields(req.body);
    if(error){
        return res.send("not validated: "+error.details[0].message);
    }


    Test.findOneAndUpdate({_id:req.body.id},{
        Question1:{
            ContenuQes1:req.body.ContenuQes1,
            PropositionsQes1:[req.body.Prop1Qes1,req.body.Prop2Qes1,req.body.Prop3Qes1],
            ReponseQes1:req.body.ReponseQes1

        },


        Question2:{
            ContenuQes2:req.body.ContenuQes2,
            PropositionsQes2:[req.body.Prop1Qes2,req.body.Prop2Qes2,req.body.Prop3Qes2],
            ReponseQes2:req.body.ReponseQes2

        },


        Question3:{
            ContenuQes3:req.body.ContenuQes3,
            PropositionsQes3:[req.body.Prop1Qes3,req.body.Prop2Qes3,req.body.Prop3Qes3],
            ReponseQes3:req.body.ReponseQes3
        }  ,


        Question4:{
            ContenuQes4:req.body.ContenuQes4,
            PropositionsQes4:[req.body.Prop1Qes4,req.body.Prop2Qes4,req.body.Prop3Qes4],
            ReponseQes4:req.body.ReponseQes4

        },


        Question5:{
            ContenuQes5:req.body.ContenuQes5,
            PropositionsQes5:[req.body.Prop1Qes5,req.body.Prop2Qes5,req.body.Prop3Qes5],
            ReponseQes5:req.body.ReponseQes5

        }
    },function(err){
        if(err){
            res.send({"state":"not updated"+err});
        }
        else{
            res.send({"state":"updated"});
        }
    })
});


router.get("/all",function(req,res){
    Test.find({},function(err,result){
        res.send(result);
    })
});


router.delete("/supprimer/:id",function(req,res){
    Test.deleteOne({_id:req.params.id},function(err){
        if(err){
            res.send({"state":"Not deleted !"+ err});
        }
        else{
            res.send({"state":"deleted!"});
        }
    });
});

function validateUpdatedFields(fields){

    const schema=({
        id:Joi.string().min(5),


        ContenuQes1:Joi.string().min(10).required(),
        Prop1Qes1:Joi.string().required(),
        Prop2Qes1:Joi.string().required(),
        Prop3Qes1:Joi.string().required(),
        ReponseQes1:Joi.string().required()

        ,

        ContenuQes2:Joi.string().min(10).required(),
        Prop1Qes2:Joi.string().required(),
        Prop2Qes2:Joi.string().required(),
        Prop3Qes2:Joi.string().required(),
        ReponseQes2:Joi.string().required()

        ,


        ContenuQes3:Joi.string().min(10).required(),
        Prop1Qes3:Joi.string().required(),
        Prop2Qes3:Joi.string().required(),
        Prop3Qes3:Joi.string().required(),
        ReponseQes3:Joi.string().required()
        ,

        ContenuQes4:Joi.string().min(10).required(),
        Prop1Qes4:Joi.string().required(),
        Prop2Qes4:Joi.string().required(),
        Prop3Qes4:Joi.string().required(),
        ReponseQes4:Joi.string().required()

        ,

        ContenuQes5:Joi.string().min(10).required(),
        Prop1Qes5:Joi.string().required(),
        Prop2Qes5:Joi.string().required(),
        Prop3Qes5:Joi.string().required(),
        ReponseQes5:Joi.string().required()





    });
    return Joi.validate(fields,schema);
}







module.exports=router;
