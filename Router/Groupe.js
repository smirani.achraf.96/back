var express=require("express")
var Router=express.Router()
var GroupeModel=require("../Models/GroupeModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all",function (req,res) {
    GroupeModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/add",function (req,res) {
    Groupe= new GroupeModel({
        nom: req.body.nom,
    })
    Groupe.save(function (err) {

        if(err)
            res.status(400).send({"state":"not ok","msg":"err"+err})
        else {
            res.status(200).send({"state":"ok","msg":"ajout"})
        }

    })
});
Router.delete("/remove/:id",function (req,res){
    GroupeModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})

Router.get('/:id', function (req, res) {
    GroupeModel.findById({_id: req.params.id}, (err, groupe)=>{
        if(!err)
            res.status(200).json(groupe)
    })
})

function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        nom:Joi.String().required().min(4)
    });
    const result=Joi.validate(fields,schema);
    return result ;
}
Router.put("/update/:id",function (req,res){
    GroupeModel.updateOne({id: req.params.id, nom: req.body.nom },function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})

Router.post("/user", function (req, res) {
    GroupeModel.findOne({_id: req.body.id}, (err, group)=> {
        if(err){
            res.status(400).end();
        }
        else{
            group.benevole.push(req.body.name);
            group.save((err, data)=>{
                if(err)
                    res.status(500).end();
                else
                    res.status(200).json(data);
            });

        }
    })
});

module.exports=Router;
