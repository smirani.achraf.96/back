var express = require("express")
var Router = express.Router()
var BenevoleModel = require("../Models/BenevoleModel")
var CitoyenModel = require("../Models/CitoyenModel")
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all", function (req, res) {
    CitoyenModel.find({}, function (err, result) {
        res.status(200).json(result);
    })
})

Router.get("/:id", function (req, res) {
    CitoyenModel.findById({_id: req.params.id}, function (err, result) {
        if(err){
            res.status(400).end();
        } else {
            res.status(200).json(result);
        }
    })
});


Router.post("/add",function (req,res) {
    citoyen= new CitoyenModel({
        nom: req.body.nom,
        prenom: req.body.prenom,
        email:req.body.email,
        mot_de_passe:req.body.mot_de_passe,
        login:req.body.login,
        adress:req.body.adress,
        avatar:req.body.avatar,
        centre_interet:req.body.centre_interet,
        evenements:req.body.evenements,
        blog:req.body.blog

    })
    citoyen.save(function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"ajout"})
        }

    })
});
Router.post("/auth/citoyen",async function (req, res) {
    await CitoyenModel.findOne({email: req.body.email}, function (err, UserInfo) {
        if (err || !UserInfo) {
            return res.status(400).end();
        } else {

            bcrypt.compare(req.body.mot_de_passe, UserInfo.mot_de_passe, (err, same) => {
                if (!err && same) {
                    const token = jwt.sign({id: UserInfo._id}, req.app.get('secretKey'), {expiresIn: '300000'});
                    res.status(200).json({
                        status: "success",
                        message: "user found ",
                        data: {admin: UserInfo, token: token}
                    })
                } else {
                    res.status(401).json({status: "error", message: "invalid email / password ", data: null});
                }
            })
        }
    })
});

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            res.json({status: "error", message: err.message, data: null});
        } else {
            // add user id to request
            req.body.UserId = decoded.id;
            next();
        }
    });

}
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        nom:Joi.String().required().min(4),
        prenom:Joi.String().required().min(4),
        mot_de_passe:Joi.String().required().min(4).trim,
        login:Joi.String().required().min(4),
        adress:Joi.String().required().min(4),
        avatar:Joi.String().min(4).trim,
        centre_interet:Joi.String().required().min(4),
        ville:Joi.String().min(4),
        blog:Joi.String().min(4),


    });
    const result=Joi.validate(fields,schema);
    return result ;
}
Router.delete("/remove/:id", function (req, res) {
    CitoyenModel.deleteOne({_id: req.params.id}, function (err) {

        if (err)
            res.send({"state": "not ok", "msg": "err" + err})
        else {
            res.send({"state": "ok", "msg": "suprimee"})
        }

    })
})
Router.put("/update", function (req, res) {
    CitoyenModel.updateOne({
            id: req.body.id, nom: req.body.nom, prenom: req.body.prenom, centre_interet: req.body.centre_interet,
            avatar:req.body.avatar, adress:req.body.adress,login:req.body.login,
            ville:req.body.ville,blog:req.body.blog,
        },
        function (err) {

            if (err)
                res.send({"state": "not ok", "msg": "err" + err})
            else {
                res.send({"state": "ok", "msg": "modifier"})
            }

        })
})
// Router.get("/citoyen",function (req,res) {
//     UtilisateurModel.find( { "type": "citoyen" },function (err,result)
//     {res.send(result)
//     })
// })

module.exports = Router;
