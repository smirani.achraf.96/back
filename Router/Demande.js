var express=require("express")
var Router=express.Router()
var DemandeModel=require("../Models/DemandeModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');
var _ = require("lodash");

Router.get("/all",function (req,res) {
    DemandeModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/signup", function (req, res) {
    try {

        var demande = _.pick(req.body, "state", "score")

        if (demande.state !== "acceptee" && demande.state !== "non acceptee") {
            return res.status(400).end()
        }
        console.log(demande);
        DemandeModel.create(demande, function (err) {

            if (err)
                res.send({"state": "not ok", "msg": "err" + err})
            else {
                res.send({"state": "ok", "msg": "ajout"})
            }

        })
    } catch (err) {
        console.log(err);
        res.status(500).end();
    }
});
Router.delete("/remove/:id",function (req,res){
    DemandeModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        nom:Joi.String().required().min(4),


    });
    const result=Joi.validate(fields,schema);
    return result ;
}
Router.put("/update/:",function (req,res){
    var {error}=validateUpdatefields(req.body);
    if(error) {
        return res.send("not validated"+error.details[0].message);
    }
    DemandeModel.updateOne({id: req.body.id, description: req.body.description },function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})
module.exports=Router;
