var express=require("express")
var Router=express.Router()
var BlogModel=require("../Models/BlogModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all",function (req,res) {
    BlogModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/add",function (req,res) {
    console.log(req.body);
    blog= new BlogModel({
        title: req.body.title,
        body:req.body.body

    })
    blog.save(function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"ajout"})
        }

    })
});
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        titre:Joi.String().required().min(4),
        auteur:Joi.String().required().min(4),



    });
    const result=Joi.validate(fields,schema);
    return result ;
}

Router.get('/:id', function (req, res) {
    BlogModel.findById({_id: req.params.id}, (err, blog)=>{
        if(!err)
            res.status(200).json(blog)
    })
})

Router.delete("/remove/:id",function (req,res){
     BlogModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})
Router.put("/update",function (req,res){
    BlogModel.updateOne({id: req.body.id, title: req.body.title, body: req.body.body },function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})
module.exports=Router;
