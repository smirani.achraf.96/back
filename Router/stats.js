var express=require("express")
var Router=express.Router()
var EvenementModel=require("../Models/EvenementModel")
var UtilisateurModel = require("../Models/UtilisateurModel")
var ProjetModel=require("../Models/ProjetModel")
var BlogModel=require("../Models/BlogModel")
var CategorieModel=require("../Models/CategorieModel")

Router.get('', async (req, res) => {
    var stats = {
        events: Number,
        citoyen: Number,
        benevole: Number,
        articles: Number,
        formation: Number,
        projects: Number
    };
   await EvenementModel.count((err, cpt)=> {
       if(err){
           res.status(500).end();
       }
       else{
           stats.events = cpt
       }
   });
    await UtilisateurModel.count({Type: 'Citoyen'}, (err, cpt)=> {
        if(err){
            res.status(500).end();
        }
        else{
            stats.citoyen = cpt
        }
    });
    await UtilisateurModel.count({Type: 'Benevole'}, (err, cpt)=> {
        if(err){
            res.status(500).end();
        }
        else{
            stats.benevole = cpt
        }
    });
    await ProjetModel.count((err, cpt)=> {
        if(err){
            res.status(500).end();
        }
        else{
            stats.projects = cpt
        }
    });
    await BlogModel.count((err, cpt)=> {
        if(err){
            res.status(500).end();
        }
        else{
            stats.articles = cpt
        }
    });
    await CategorieModel.count((err, cpt)=> {
        if(err){
            res.status(500).end();
        }
        else{
            stats.formation = cpt
        }
    });
    console.log(stats);
    res.status(200).json(stats);
});

module.exports=Router;
