var express=require("express")
var Router=express.Router()
var ProjetModel=require("../Models/ProjetModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all",function (req,res) {
    ProjetModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/add",function (req,res) {
    projet= new ProjetModel({
        titre: req.body.titre,
        lien: req.body.lien,
        description: req.body.description,
        evenement:req.body.evenement

    }),
        projet.save(function (err) {

            if(err)
                res.send({"state":"not ok","msg":"err"+err})
            else {
                res.send({"state":"ok","msg":"ajout"})
            }

        })
});

Router.delete("/remove/:id",function (req,res){
    ProjetModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.status(400).send({"state":"not ok","msg":"err"+err})
        else {
            res.status(200).send({"state":"ok","msg":"suprimee"})
        }

    })
})
Router.put("/update",function (req,res){

    ProjetModel.updateOne({id: req.body.id,titre: req.body.titre, lien: req.body.lien, description: req.body.description },function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})
module.exports=Router;
