var express = require("express")
var Router = express.Router()
var BenevoleModel = require("../Models/BenevoleModel")
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all", function (req, res) {
    BenevoleModel.find({}, function (err, result) {
        res.send((result))
    })
});

Router.get("/:id", function (req, res) {
    BenevoleModel.findById({_id: req.params.id}, function (err, result) {
        if(err){
            res.status(400).end();
        } else {
            res.status(200).json(result);
        }
    })
});

Router.post("/add",function (req,res) {
    benevole= new BenevoleModel({
        nom: req.body.nom,
        prenom: req.body.prenom,
        email:req.body.email,
        mot_de_passe:req.body.mot_de_passe,
        login:req.body.login,
        adress:req.body.adress,
        avatar:req.body.avatar,
        competence:req.body.competence,
        evenements:req.body.evenements,
        blog:req.body.blog,
        description: req.body.description,
        ville:req.body.ville,
        // ville:req.body.ville,
        // blog:req.body.blog

    })
    benevole.save(function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"ajout"})
        }

    })
});

Router.post("/auth/benevole",async function (req, res) {
    await BenevoleModel.findOne({email: req.body.email}, function (err, UserInfo) {
        if (err || !UserInfo) {
            return res.status(400).end();
        } else {

            bcrypt.compare(req.body.mot_de_passe, UserInfo.mot_de_passe, (err, same) => {
                if (!err && same) {
                    const token = jwt.sign({id: UserInfo._id}, req.app.get('secretKey'), {expiresIn: '300000'});
                    res.status(200).json({
                        status: "success",
                        message: "user found ",
                        data: {admin: UserInfo, token: token}
                    })
                } else {
                    console.log(err)
                    res.status(401).json({status: "error", message: "invalid email / password ", data: null});
                }
            })
        }
    })
});


function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
        if (err) {
            res.json({status: "error", message: err.message, data: null});
        } else {
            // add user id to request
            req.body.UserId = decoded.id;
            next();
        }
    });

}
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        nom:Joi.String().required().min(4),
        prenom:Joi.String().required().min(4),
        mot_de_passe:Joi.String().required().min(4).trim,
        login:Joi.String().required().min(4),
        adress:Joi.String().required().min(4),
        avatar:Joi.String().min(4).trim,
        competence:Joi.String().required().min(4),
        ville:Joi.String().min(4),
        blog:Joi.String().min(4),


    });
    const result=Joi.validate(fields,schema);
    return result ;
}


Router.delete("/remove/:id", function (req, res) {
    BenevoleModel.deleteOne({_id: req.params.id}, function (err) {

        if (err)
            res.send({"state": "not ok", "msg": "err" + err})
        else {
            res.send({"state": "ok", "msg": "suprimee"})
        }

    })
})
Router.put("/update", function (req, res) {
    BenevoleModel.updateOne({
            id: req.body.id, nom: req.body.nom, prenom: req.body.prenom, centre_interet: req.body.centre_interet,
            avatar:req.body.avatar, adress:req.body.adress,login:req.body.login,
            ville:req.body.ville,blog:req.body.blog,
        },
        function (err) {

            if (err)
                res.send({"state": "not ok", "msg": "err" + err})
            else {
                res.send({"state": "ok", "msg": "modifier"})
            }

        })
})
// Router.get("/citoyen",function (req,res) {
//     UtilisateurModel.find( { "type": "citoyen" },function (err,result)
//     {res.send(result)
//     })
// })

module.exports = Router;
