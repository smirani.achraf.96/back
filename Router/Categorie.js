var express=require("express")
var Router=express.Router()
var CategorieModel=require("../Models/CategorieModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all",function (req,res) {
    CategorieModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/add",function (req,res) {
    categorie= new CategorieModel({
        titre: req.body.titre,
        lieu: req.body.lieu,
        description: req.body.description,

    }),
    categorie.save(function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"ajout"})
        }

    })
});
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        description:Joi.String().required().min(4),
        evenement:Joi.String().required().min(4),


    });
    const result=Joi.validate(fields,schema);
    return result ;
}
Router.delete("/remove/:id",function (req,res){
    CategorieModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})
Router.put("/update",function (req,res){
    CategorieModel.updateOne({id: req.body.id,titre: req.body.titre, lieu: req.body.lieu, description: req.body.description },function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})
module.exports=Router;
