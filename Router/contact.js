var express=require("express")
var Router=express.Router()
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');
var ContactModel=require("../Models/ContactModel")

Router.get("/all",function (req,res) {
    ContactModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/add",function (req,res) {
    Contact= new ContactModel({
        titre: req.body.titre,
        msg:req.body.msg,
        Utilisateur: req.body.utilisateur
    })
    Contact.save(function (err) {

        if(err)
            res.status(400).send({"state":"not ok","msg":"err"+err})
        else {
            res.status(200).send({"state":"ok","msg":"ajout"})
        }

    })
});
Router.delete("/remove/:id",function (req,res){
    ContactModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        description:Joi.String().required().min(4),


    });
    const result=Joi.validate(fields,schema);
    return result ;
}

module.exports=Router;
