var express=require("express")
var Router=express.Router()
var VilleModel=require("../Models/VilleModel")
const bcrypt= require('bcrypt')
const jwt=require('jsonwebtoken')
const Joi = require('joi');

Router.get("/all",function (req,res) {
    VilleModel.find({},function (errr,result) {
        res.send((result))
    })
})
Router.post("/add",function (req,res) {
    ville= new VilleModel({
        description: req.body.description,
        region:req.body.region,

    })
    ville.save(function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"ajout"})
        }

    })
});
Router.delete("/remove/:id",function (req,res){
    VilleModel.deleteOne({_id:req.params.id},function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"suprimee"})
        }

    })
})
function validateUpdatefields(fields) {
    const schema=({
        id:Joi.String().min(5),
        description:Joi.String().required().min(4),


    });
    const result=Joi.validate(fields,schema);
    return result ;
}
Router.put("/update/:",function (req,res){
    var {error}=validateUpdatefields(req.body);
    if(error) {
        return res.send("not validated"+error.details[0].message);
    }
    VilleModel.updateOne({id: req.body.id, description: req.body.description },function (err) {

        if(err)
            res.send({"state":"not ok","msg":"err"+err})
        else {
            res.send({"state":"ok","msg":"modifier"})
        }

    })
})
module.exports=Router;
