var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var EvenementModelSchema = new Schema({
    titre: { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    lieu: { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    detail: { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    nb_participant: { type: Number, default: 0} ,
     benevole:
    {type: Schema.Types.ObjectId, ref:"BenevoleModel"}
,
    categorie:
        {type: Schema.Types.ObjectId, ref:"CategorieModel"}
    ,
    citoyen:[
        {type: Schema.Types.ObjectId, ref:"CitoyenModel"}
    ],
    images:
        {type: String}

});

// Compile model from schema
var EvenementModel = mongoose.model('EvenementModel', EvenementModelSchema );
module.exports=EvenementModel;
