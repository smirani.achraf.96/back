const user = require ("./UtilisateurModel");
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


const AdminSchema = user.discriminator("Admin",new Schema({
    role:{
        type: String,
        trim: true, // elimine les espaces ,
    },
    groupe:  [{type: Schema.Types.ObjectId, ref:"GroupeModel"}],
    demande:[{type: Schema.Types.ObjectId, ref:"DemandeModel"}]


}))

var AdminModel = mongoose.model('Admin' );
module.exports=AdminModel;
