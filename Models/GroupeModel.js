var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var GroupeModelSchema = new Schema({
    nom:  { type: String,
        required:true } ,
    benevole: [
        {type: String}
    ],

});

// Compile model from schema
var GroupeModel= mongoose.model('GroupeModel',GroupeModelSchema);
module.exports=GroupeModel;
