var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;
var CompetenceModelSchema = new Schema({

    description:  { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    Benevole:[{type:Schema.Types.ObjectId, ref:'BenevoleModel'}],
})
var CompetenceModel= mongoose.model('CompetenceModel',CompetenceModelSchema);
module.exports=CompetenceModel;