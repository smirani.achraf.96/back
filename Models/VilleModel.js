var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var VilleModelSchema = new Schema({
    description:  { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    region: [
        {type: Schema.Types.ObjectId, ref:"RegionModel"}
    ],

})
{ timestamps: true
};

// Compile model from schema
var VilleModel= mongoose.model('VilleModel',VilleModelSchema);
module.exports=VilleModel;