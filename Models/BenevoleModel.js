const user = require ("./UtilisateurModel");
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


const BenevoleSchema = user.discriminator("Benevole",new Schema({
    description:{
        type: String
    },
    evenements: [
    {type: Schema.Types.ObjectId, ref:"EvenementModel"}
],
    blog: [
        {type: Schema.Types.ObjectId, ref:"BlogModel"}
    ],
    competence: [
        {type: Schema.Types.ObjectId, ref:"CompetenceModel"}
    ],
    demande:{type: Schema.Types.ObjectId, ref:"DemandeModel"},
    groupe:{type: Schema.Types.ObjectId, ref:"GroupeModel"}


}))

var BenevoleModel = mongoose.model('Benevole' );
module.exports=BenevoleModel;
