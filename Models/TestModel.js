const mongoose=require("mongoose");
//require('mongoose-type-url');

const Schema=mongoose.Schema;


const TestModelSchema = new Schema({



    Question1:{
        ContenuQes1:{type:String,minlength:10,required:true},
        PropositionsQes1:[{
            type:String,required:true

        }],
        ReponseQes1:{type:String,required:true}

    },


    Question2:{
        ContenuQes2:{type:String,minlength:10,required:true},
        PropositionsQes2:[{
            type:String,required:true

        }],
        ReponseQes2:{type:String,required:true}

    },


    Question3:{
        ContenuQes3:{type:String,minlength:10,required:true},
        PropositionsQes3:[{
            type:String,required:true

        }],
        ReponseQes3:{type:String,required:true}

    }  ,


    Question4:{
        ContenuQes4:{type:String,minlength:10,required:true},
        PropositionsQes4:[{
            type:String,required:true

        }],
        ReponseQes4:{type:String,required:true}

    },


    Question5:{
        ContenuQes5:{type:String,minlength:10,required:true},
        PropositionsQes5:[{
            type:String,required:true

        }],
        ReponseQes5:{type:String,required:true}

    },

    Benevole:[{type:Schema.Types.ObjectId, ref:'BenevoleModel'}],




},
    {
    timestamps:true
});


const TestModel=mongoose.model("TestModel",TestModelSchema);
module.exports=TestModel;
