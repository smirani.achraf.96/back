const user = require ("./UtilisateurModel");
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


const CitoyenSchema = user.discriminator("Citoyen",new Schema({
        description:{
            type: String
        },
    evenements: [
        {type: Schema.Types.ObjectId, ref:"EvenementModel"}
    ],

    blog: [
    {type: Schema.Types.ObjectId, ref:"BlogModel"}
],
    centre_interet: [
        {type: Schema.Types.ObjectId, ref:"Centre_interetModel"}
    ],
    }))

var CitoyenModel = mongoose.model('Citoyen' );
module.exports=CitoyenModel;
