var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;
var ContactModelSchema = new Schema({

    titre:  { type: String,
        required:true } ,
    msg:  { type: String,
        required:true } ,
    Utilisateur:{type: String, required: true},
})
var ContactModel= mongoose.model('ContactModel',ContactModelSchema);
module.exports=ContactModel;
