var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var CategorieModelSchema = new Schema({
        description:  { type: String,
        required:true } ,
    titre: {type: String},
    lieu: {type: String},
    evenement: [
        {type: Schema.Types.ObjectId, ref:"EvenementModel"}
    ],

});

// Compile model from schema
var CategorieModel= mongoose.model('CategorieModel',CategorieModelSchema);
module.exports=CategorieModel;
