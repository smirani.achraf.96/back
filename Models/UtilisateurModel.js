var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Joi = require('joi');

const saltRounds = 10 ;
var Schema = mongoose.Schema;
var UtilisateurModelSchema = new Schema({
    nom: {
        type: String,
        trim: true, // elimine les espaces ,
        required: true
    }, // obligatoires
    prenom: {
        type: String,
        trim: true, // elimine les espaces ,
        required: true
    }, // obligatoires
    mot_de_passe: {
        type: String,
        required: true
    }, // obligatoires

    email: {
        type: String,
        // email:true,
        pattern: "^.+\@.+$",
        trim: true,
        required: true
    },
    login: {
        type: String,
        trim: true,
        required: true

    },
    avatar:{
        type:String,
        trim: true,
        required: false
    },
    // type: ["benevole","citoyen"],
    //
    // centre_interet: {
    //     type: String,
    //     trim: true,
    //     required: false
    // },
    //
    // competence: {
    //     type: String,
    //     trim: true,
    //     required: false
    // },
        region:
            {type: Schema.Types.ObjectId,ref:"RegionModel"}
        ,
    blog: [
    {type: Schema.Types.ObjectId,ref:"BlogModel"} ]
    },
    {discriminatorKey:"Type" ,
        timestamps: true});






UtilisateurModelSchema.pre('save',function (next) {
    this.mot_de_passe = bcrypt.hashSync(this.mot_de_passe,saltRounds);
    next();

})
// Compile Model from schema
var UtilisateurModel = mongoose.model('UtilisateurModel', UtilisateurModelSchema );
module.exports=UtilisateurModel;
