var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var RegionModelSchema = new Schema({
    description:  { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    utilisateur: [
        {type: Schema.Types.ObjectId, ref:"UtilisateurModel"}
    ],

})
{ timestamps: true
};

// Compile model from schema
var RegionModel= mongoose.model('RegionModel',RegionModelSchema);
module.exports=RegionModel;