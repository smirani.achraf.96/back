var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var ProjetModelSchema = new Schema({
    titre: { type: String } ,
    lien: { type: String } ,
    description:  { type: String } ,
    evenement: [
        {type: Schema.Types.ObjectId, ref:"EvenementModel"}
    ],

});

// Compile model from schema
var ProjetModel= mongoose.model('ProjetModel',ProjetModelSchema);
module.exports=ProjetModel;
