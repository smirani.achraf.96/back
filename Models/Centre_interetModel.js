var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;
var Centre_interetModelSchema = new Schema({

    description:  { type: String,
        trim: true, // elimine les espaces ,
        required:true } ,
    Citoyen:[{type:Schema.Types.ObjectId, ref:'CitoyenModel'}],

})
var Centre_interetModel= mongoose.model('Centre_interetModel',Centre_interetModelSchema);
module.exports=Centre_interetModel;