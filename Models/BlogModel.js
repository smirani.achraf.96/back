var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var BlogModelSchema = new Schema({
    title:  {
        type: String,
        required:true } ,
    auteur:
        {type: String},

    body: {type: String}

});

// Compile model from schema
var BlogModel= mongoose.model('BlogModel',BlogModelSchema);
module.exports=BlogModel;
