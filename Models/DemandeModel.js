var mongoose = require('mongoose');
// Define schema
var Schema = mongoose.Schema;

var DemandeModelSchema = new Schema({
    state:   ["acceptee","non acceptee"],

    score:  {type: Schema.Types.ObjectId, ref:"TestModel"},
    benevole:[{type: Schema.Types.ObjectId, ref:"BenevoleModel"}]


});

// Compile model from schema
var DemandeModel= mongoose.model('DemandeModel',DemandeModelSchema);
module.exports=DemandeModel;